package response

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"source.toby3d.me/toby3d/auth/internal/common"
)

// NOTE(toby3d): Encapsulate enums in structs for extra compile-time safety:
// https://threedots.tech/post/safer-enums-in-go/#struct-based-enums
type Type struct {
	responseType string
}

//nolint:gochecknoglobals // structs cannot be constants
var (
	Und = Type{} // "und"

	// ID indicates to the authorization server that this is an
	// authentication request. If this parameter is missing, the
	// authorization endpoint MUST default to id.
	//
	// Deprecated: Only accept response_type=code requests, and for
	// backwards-compatible support, treat response_type=id requests as
	// response_type=code requests:
	// https://aaronparecki.com/2020/12/03/1/indieauth-2020#response-type
	ID = Type{"id"} // "id"

	// Code indicates to the authorization server that an
	// authorization code should be returned as the response:
	// https://indieauth.net/source/#authorization-request-li-1
	Code = Type{"code"} // "code"
)

var ErrResponseTypeUnknown error = errors.New("unknown grant type")

// ParseType parse string as response type struct enum.
func ParseType(uid string) (Type, error) {
	switch strings.ToLower(uid) {
	case Code.responseType:
		return Code, nil
	case ID.responseType:
		return ID, nil
	}

	return Und, fmt.Errorf("%w: %s", ErrResponseTypeUnknown, uid)
}

// UnmarshalForm implements custom unmarshler for form values.
func (t *Type) UnmarshalForm(src []byte) error {
	responseType, err := ParseType(string(src))
	if err != nil {
		return fmt.Errorf("ResponseType: UnmarshalForm: %w", err)
	}

	*t = responseType

	return nil
}

// UnmarshalJSON implements custom unmarshler for JSON.
func (t *Type) UnmarshalJSON(v []byte) error {
	unquoted, err := strconv.Unquote(string(v))
	if err != nil {
		return fmt.Errorf("ResponseType: UnmarshalJSON: cannot unquote value '%s': %w", string(v), err)
	}

	responseType, err := ParseType(strings.ToLower(unquoted))
	if err != nil {
		return fmt.Errorf("ResponseType: UnmarshalJSON: cannot parse value '%s': %w", unquoted, err)
	}

	*t = responseType

	return nil
}

func (t Type) MarshalJSON() ([]byte, error) {
	return []byte(strconv.Quote(t.responseType)), nil
}

// String returns string representation of response type.
func (t Type) String() string {
	if t == Und {
		return common.Und
	}

	return t.responseType
}

func (t Type) GoString() string {
	return "response.Type(" + t.String() + ")"
}
