package response_test

import (
	"testing"

	"source.toby3d.me/toby3d/auth/internal/domain/response"
)

func TestParseType(t *testing.T) {
	t.Parallel()

	for name, tc := range map[string]struct {
		input  string
		expect response.Type
	}{
		"id":   {input: "id", expect: response.ID},
		"code": {input: "code", expect: response.Code},
	} {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			actual, err := response.ParseType(tc.input)
			if err != nil {
				t.Fatal(err)
			}

			if actual != tc.expect {
				t.Errorf("ParseResponseType(%s) = %v, want %v", tc.input, actual, tc.expect)
			}
		})
	}
}

func TestType_UnmarshalForm(t *testing.T) {
	t.Parallel()

	input := []byte("code")
	actual := response.Und

	if err := actual.UnmarshalForm(input); err != nil {
		t.Fatal(err)
	}

	if actual != response.Code {
		t.Errorf("UnmarshalForm(%s) = %v, want %v", input, actual, response.Code)
	}
}

func TestType_UnmarshalJSON(t *testing.T) {
	t.Parallel()

	input := []byte(`"code"`)
	actual := response.Und

	if err := actual.UnmarshalJSON(input); err != nil {
		t.Fatal(err)
	}

	if actual != response.Code {
		t.Errorf("UnmarshalJSON(%s) = %v, want %v", input, actual, response.Code)
	}
}

func TestType_String(t *testing.T) {
	t.Parallel()

	for name, tc := range map[string]struct {
		input  response.Type
		expect string
	}{
		"id":   {input: response.ID, expect: "id"},
		"code": {input: response.Code, expect: "code"},
	} {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			if actual := tc.input.String(); actual != tc.expect {
				t.Errorf("String() = %s, want %s", actual, tc.expect)
			}
		})
	}
}
