package challenge

//nolint:gosec // support old clients
import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"errors"
	"fmt"
	"hash"
	"strconv"
	"strings"

	"source.toby3d.me/toby3d/auth/internal/common"
)

// Method represent a PKCE challenge method for validate verifier.
//
// NOTE(toby3d): Encapsulate enums in structs for extra compile-time safety:
// https://threedots.tech/post/safer-enums-in-go/#struct-based-enums
type Method struct {
	codeChallengeMethod string
}

//nolint:gochecknoglobals // structs cannot be constants
var (
	Und   = Method{}        // "und"
	PLAIN = Method{"plain"} // "plain"
	MD5   = Method{"md5"}   // "md5"
	S1    = Method{"s1"}    // "s1"
	S256  = Method{"s256"}  // "s256"
	S512  = Method{"s512"}  // "s512"
)

var ErrCodeChallengeMethodUnknown error = errors.New("unknown code_challenge_method")

//nolint:gochecknoglobals // maps cannot be constants
var uidsMethods = map[string]Method{
	MD5.codeChallengeMethod:   MD5,
	PLAIN.codeChallengeMethod: PLAIN,
	S1.codeChallengeMethod:    S1,
	S256.codeChallengeMethod:  S256,
	S512.codeChallengeMethod:  S512,
}

// Parse parse string identifier of code challenge method into struct enum.
func Parse(uid string) (Method, error) {
	if method, ok := uidsMethods[uid]; ok {
		return method, nil
	}

	return Und, fmt.Errorf("%w: %s", ErrCodeChallengeMethodUnknown, uid)
}

// UnmarshalForm implements custom unmarshler for form values.
func (m *Method) UnmarshalForm(v []byte) error {
	parsed, err := Parse(strings.ToLower(string(v)))
	if err != nil {
		return fmt.Errorf("CodeChallengeMethod: UnmarshalForm: %w", err)
	}

	*m = parsed

	return nil
}

// UnmarshalJSON implements custom unmarshler for JSON.
func (m *Method) UnmarshalJSON(v []byte) error {
	unquoted, err := strconv.Unquote(string(v))
	if err != nil {
		return fmt.Errorf("CodeChallengeMethod: UnmarshalJSON: cannot unquote value '%s': %w", string(v), err)
	}

	parsed, err := Parse(strings.ToLower(unquoted))
	if err != nil && !errors.Is(err, ErrCodeChallengeMethodUnknown) {
		return fmt.Errorf("CodeChallengeMethod: UnmarshalJSON: cannot parse '%s' value: %w", unquoted, err)
	}

	*m = parsed

	return nil
}

func (m Method) MarshalJSON() ([]byte, error) {
	if m == Und {
		return nil, nil
	}

	return []byte(strconv.Quote(m.String())), nil
}

// String returns string representation of code challenge method.
func (m Method) String() string {
	if m == Und {
		return common.Und
	}

	return strings.ToUpper(m.codeChallengeMethod)
}

func (m Method) GoString() string {
	return "challenge.Method(" + m.String() + ")"
}

// Validate checks for a match to the verifier with the hashed version of the
// challenge via the chosen method.
func (m Method) Validate(codeChallenge, verifier string) bool {
	var h hash.Hash

	switch m {
	default:
		return false
	case PLAIN:
		return codeChallenge == verifier
	case MD5:
		h = md5.New()
	case S1:
		h = sha1.New()
	case S256:
		h = sha256.New()
	case S512:
		h = sha512.New()
	}

	if _, err := h.Write([]byte(verifier)); err != nil {
		return false
	}

	return codeChallenge == base64.RawURLEncoding.EncodeToString(h.Sum(nil))
}
