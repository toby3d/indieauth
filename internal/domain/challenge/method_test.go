package challenge_test

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"hash"
	"testing"

	"github.com/brianvoe/gofakeit/v6"

	"source.toby3d.me/toby3d/auth/internal/domain/challenge"
	"source.toby3d.me/toby3d/auth/internal/random"
)

func TestParse(t *testing.T) {
	t.Parallel()

	for name, tc := range map[string]struct {
		input  string
		expect challenge.Method
	}{
		"PLAIN": {input: "plain", expect: challenge.PLAIN},
		"MD5":   {input: "md5", expect: challenge.MD5},
		"S1":    {input: "s1", expect: challenge.S1},
		"S256":  {input: "s256", expect: challenge.S256},
		"S512":  {input: "s512", expect: challenge.S512},
	} {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			actual, err := challenge.Parse(tc.input)
			if err != nil {
				t.Fatal(err)
			}

			if actual != tc.expect {
				t.Errorf("Parse(%s) = %v, want %v", tc.input, actual, tc.expect)
			}
		})
	}
}

func TestMethod_UnmarshalForm(t *testing.T) {
	t.Parallel()

	input := []byte("s256")
	actual := challenge.Und

	if err := actual.UnmarshalForm(input); err != nil {
		t.Fatal(err)
	}

	if actual != challenge.S256 {
		t.Errorf("UnmarshalForm(%s) = %v, want %v", input, actual, challenge.S256)
	}
}

func TestMethod_UnmarshalJSON(t *testing.T) {
	t.Parallel()

	input := []byte(`"S256"`)
	actual := challenge.Und

	if err := actual.UnmarshalJSON(input); err != nil {
		t.Fatal(err)
	}

	if actual != challenge.S256 {
		t.Errorf("UnmarshalJSON(%s) = %v, want %v", input, actual, challenge.S256)
	}
}

func TestMethod_String(t *testing.T) {
	t.Parallel()

	for name, tc := range map[string]struct {
		input  challenge.Method
		expect string
	}{
		"plain": {input: challenge.PLAIN, expect: "PLAIN"},
		"md5":   {input: challenge.MD5, expect: "MD5"},
		"s1":    {input: challenge.S1, expect: "S1"},
		"s256":  {input: challenge.S256, expect: "S256"},
		"s512":  {input: challenge.S512, expect: "S512"},
	} {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			if actual := tc.input.String(); actual != tc.expect {
				t.Errorf("String() = %v, want %v", actual, tc.expect)
			}
		})
	}
}

//nolint:gosec // support old clients
func TestMethod_Validate(t *testing.T) {
	t.Parallel()

	verifier, err := random.String(uint8(gofakeit.Number(43, 128)))
	if err != nil {
		t.Fatal(err)
	}

	for name, tc := range map[string]struct {
		hash  hash.Hash
		input challenge.Method
		ok    bool
	}{
		"invalid": {input: challenge.S256, hash: md5.New(), ok: true},
		"MD5":     {input: challenge.MD5, hash: md5.New(), ok: false},
		"plain":   {input: challenge.PLAIN, hash: nil, ok: false},
		"S1":      {input: challenge.S1, hash: sha1.New(), ok: false},
		"S256":    {input: challenge.S256, hash: sha256.New(), ok: false},
		"S512":    {input: challenge.S512, hash: sha512.New(), ok: false},
		"Und":     {input: challenge.Und, hash: nil, ok: true},
	} {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			var codeChallenge string

			switch tc.input {
			case challenge.Und, challenge.PLAIN:
				codeChallenge = verifier
			default:
				hash := tc.hash
				hash.Reset()

				if _, err := hash.Write([]byte(verifier)); err != nil {
					t.Error(err)
				}

				codeChallenge = base64.RawURLEncoding.EncodeToString(hash.Sum(nil))
			}

			if actual := tc.input.Validate(codeChallenge, verifier); actual != !tc.ok {
				t.Errorf("Validate(%s, %s) = %t, want %t", codeChallenge, verifier, actual, tc.ok)
			}
		})
	}
}

func TestMethod_Validate_IndieAuth(t *testing.T) {
	t.Parallel()

	if ok := challenge.S256.Validate(
		"ALiMNf5FvF_LIWLhSkd9tjPKh3PEmai2OrdDBzrVZ3M",
		"6f535c952339f0670311b4bbec5c41c00805e83291fc7eb15ca4963f82a4d57595787dcc6ee90571fb7789cbd521fe0178ed",
	); !ok {
		t.Errorf("Validate(%s, %s) = %t, want %t", "ALiMNf5FvF_LIWLhSkd9tjPKh3PEmai2OrdDBzrVZ3M",
			"6f535c952339f0670311b4bbec5c41c00805e83291fc7eb15ca4963f82a4d57595787dcc6ee90571fb7789cbd521"+
				"fe0178ed", ok, true)
	}
}
