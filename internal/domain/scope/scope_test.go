package scope_test

import (
	"testing"

	"source.toby3d.me/toby3d/auth/internal/domain/scope"
)

func TestParseScope(t *testing.T) {
	t.Parallel()

	for name, tc := range map[string]struct {
		input  string
		expect scope.Scope
	}{
		"create":   {input: "create", expect: scope.Create},
		"delete":   {input: "delete", expect: scope.Delete},
		"draft":    {input: "draft", expect: scope.Draft},
		"media":    {input: "media", expect: scope.Media},
		"update":   {input: "update", expect: scope.Update},
		"block":    {input: "block", expect: scope.Block},
		"channels": {input: "channels", expect: scope.Channels},
		"follow":   {input: "follow", expect: scope.Follow},
		"mute":     {input: "mute", expect: scope.Mute},
		"read":     {input: "read", expect: scope.Read},
		"profile":  {input: "profile", expect: scope.Profile},
		"email":    {input: "email", expect: scope.Email},
	} {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			actual, err := scope.Parse(tc.input)
			if err != nil {
				t.Fatal(err)
			}

			if actual != tc.expect {
				t.Errorf("ParseScope(%s) = %v, want %v", tc.input, actual, tc.expect)
			}
		})
	}
}

func TestScope_String(t *testing.T) {
	t.Parallel()

	for name, tc := range map[string]struct {
		input  scope.Scope
		expect string
	}{
		"create":   {input: scope.Create, expect: "create"},
		"delete":   {input: scope.Delete, expect: "delete"},
		"draft":    {input: scope.Draft, expect: "draft"},
		"media":    {input: scope.Media, expect: "media"},
		"update":   {input: scope.Update, expect: "update"},
		"block":    {input: scope.Block, expect: "block"},
		"channels": {input: scope.Channels, expect: "channels"},
		"follow":   {input: scope.Follow, expect: "follow"},
		"mute":     {input: scope.Mute, expect: "mute"},
		"read":     {input: scope.Read, expect: "read"},
		"profile":  {input: scope.Profile, expect: "profile"},
		"email":    {input: scope.Email, expect: "email"},
	} {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			if result := tc.input.String(); result != tc.expect {
				t.Errorf("String() = %s, want %s", result, tc.expect)
			}
		})
	}
}
