package grant_test

import (
	"testing"

	"source.toby3d.me/toby3d/auth/internal/domain/grant"
)

func TestParseType(t *testing.T) {
	t.Parallel()

	for name, tc := range map[string]struct {
		input  string
		expect grant.Type
	}{
		"authorization_code": {input: "authorization_code", expect: grant.AuthorizationCode},
		"ticket":             {input: "ticket", expect: grant.Ticket},
	} {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			actual, err := grant.ParseType(tc.input)
			if err != nil {
				t.Fatal(err)
			}

			if actual != tc.expect {
				t.Errorf("ParseType(%s) = %v, want %v", tc.input, actual, tc.expect)
			}
		})
	}
}

func TestType_UnmarshalForm(t *testing.T) {
	t.Parallel()

	input := []byte("authorization_code")
	actual := grant.Und

	if err := actual.UnmarshalForm(input); err != nil {
		t.Fatal(err)
	}

	if actual != grant.AuthorizationCode {
		t.Errorf("UnmarshalForm(%s) = %v, want %v", input, actual, grant.AuthorizationCode)
	}
}

func TestType_UnmarshalJSON(t *testing.T) {
	t.Parallel()

	input := []byte(`"authorization_code"`)
	actual := grant.Und

	if err := actual.UnmarshalJSON(input); err != nil {
		t.Fatal(err)
	}

	if actual != grant.AuthorizationCode {
		t.Errorf("UnmarshalJSON(%s) = %v, want %v", input, actual, grant.AuthorizationCode)
	}
}

func TestType_String(t *testing.T) {
	t.Parallel()

	for name, tc := range map[string]struct {
		input  grant.Type
		expect string
	}{
		"authorization_code": {input: grant.AuthorizationCode, expect: "authorization_code"},
		"ticket":             {input: grant.Ticket, expect: "ticket"},
	} {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			if actual := tc.input.String(); actual != tc.expect {
				t.Errorf("String() = %v, want %v", actual, tc.expect)
			}
		})
	}
}
