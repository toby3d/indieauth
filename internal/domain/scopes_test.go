package domain_test

import (
	"fmt"
	"reflect"
	"testing"

	"source.toby3d.me/toby3d/auth/internal/domain"
	"source.toby3d.me/toby3d/auth/internal/domain/scope"
)

func TestScopes_UnmarshalForm(t *testing.T) {
	t.Parallel()

	input := []byte("profile email")
	actual := make(domain.Scopes, 0)

	if err := actual.UnmarshalForm(input); err != nil {
		t.Fatal(err)
	}

	expect := domain.Scopes{scope.Profile, scope.Email}
	if !reflect.DeepEqual(actual, expect) {
		t.Errorf("UnmarshalForm(%s) = %s, want %s", input, actual, expect)
	}
}

func TestScopes_UnmarshalJSON(t *testing.T) {
	t.Parallel()

	input := []byte(`"profile email"`)
	actual := make(domain.Scopes, 0)

	if err := actual.UnmarshalJSON(input); err != nil {
		t.Fatal(err)
	}

	expect := domain.Scopes{scope.Profile, scope.Email}
	if !reflect.DeepEqual(actual, expect) {
		t.Errorf("UnmarshalJSON(%s) = %s, want %s", input, actual, expect)
	}
}

func TestScopes_MarshalJSON(t *testing.T) {
	t.Parallel()

	scopes := domain.Scopes{scope.Email, scope.Profile}

	actual, err := scopes.MarshalJSON()
	if err != nil {
		t.Fatal(err)
	}

	if string(actual) != fmt.Sprintf(`"%s"`, scopes) {
		t.Errorf("MarshalJSON() = %s, want %s", actual, fmt.Sprintf(`"%s"`, scopes))
	}
}

func TestScopes_String(t *testing.T) {
	t.Parallel()

	scopes := domain.Scopes{scope.Profile, scope.Email}
	if actual := scopes.String(); actual != fmt.Sprint(scopes) {
		t.Errorf("String() = %s, want %s", actual, scopes)
	}
}

func TestScopes_IsEmpty(t *testing.T) {
	t.Parallel()

	scopes := domain.Scopes{scope.Und}
	if actual := scopes.IsEmpty(); !actual {
		t.Errorf("IsEmpty() = %t, want %t", actual, true)
	}
}

func TestScopes_Has(t *testing.T) {
	t.Parallel()

	scopes := domain.Scopes{scope.Profile, scope.Email}
	if actual := scopes.Has(scope.Email); !actual {
		t.Errorf("Has(%s) = %t, want %t", scope.Email, actual, true)
	}
}
