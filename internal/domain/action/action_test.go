package action_test

import (
	"testing"

	"source.toby3d.me/toby3d/auth/internal/domain/action"
)

func TestParse(t *testing.T) {
	t.Parallel()

	for name, tc := range map[string]struct {
		input  string
		expect action.Action
	}{
		"revoke": {input: "revoke", expect: action.Revoke},
		"ticket": {input: "ticket", expect: action.Ticket},
	} {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			actual, err := action.Parse(tc.input)
			if err != nil {
				t.Fatal(err)
			}

			if actual != tc.expect {
				t.Errorf("ParseAction(%s) = %v, want %v", tc.input, actual, tc.expect)
			}
		})
	}
}

func TestAction_UnmarshalForm(t *testing.T) {
	t.Parallel()

	input := []byte("revoke")
	actual := action.Und

	if err := actual.UnmarshalForm(input); err != nil {
		t.Fatal(err)
	}

	if actual != action.Revoke {
		t.Errorf("UnmarshalForm(%s) = %v, want %v", input, actual, action.Revoke)
	}
}

func TestAction_UnmarshalJSON(t *testing.T) {
	t.Parallel()

	input := []byte(`"revoke"`)
	actual := action.Und

	if err := actual.UnmarshalJSON(input); err != nil {
		t.Fatal(err)
	}

	if actual != action.Revoke {
		t.Errorf("UnmarshalJSON(%s) = %v, want %v", input, actual, action.Revoke)
	}
}

func TestAction_String(t *testing.T) {
	t.Parallel()

	for name, tc := range map[string]struct {
		input  action.Action
		expect string
	}{
		"revoke": {input: action.Revoke, expect: "revoke"},
		"ticket": {input: action.Ticket, expect: "ticket"},
	} {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			if actual := tc.input.String(); actual != tc.expect {
				t.Errorf("String() = %v, want %v", actual, tc.expect)
			}
		})
	}
}
