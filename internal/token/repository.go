package token

import (
	"context"

	"source.toby3d.me/toby3d/auth/internal/domain"
)

type (
	Repository interface {
		Create(ctx context.Context, accessToken domain.Token) error
		Get(ctx context.Context, accessToken string) (*domain.Token, error)
	}

	dummyTokenRepository struct{}

	stubTokenRepository struct {
		token *domain.Token
		error error
	}
)

var (
	ErrExist    error = domain.NewError(domain.ErrorCodeServerError, "token already exist", "")
	ErrNotExist error = domain.NewError(domain.ErrorCodeServerError, "token not exist", "")
)

func NewDummyTokenRepository() Repository {
	return dummyTokenRepository{}
}

func (dummyTokenRepository) Create(_ context.Context, _ domain.Token) error {
	return nil
}

func (dummyTokenRepository) Get(_ context.Context, _ string) (*domain.Token, error) {
	return nil, nil
}

func NewStubTokenRepository(token *domain.Token, err error) Repository {
	return &stubTokenRepository{
		token: token,
		error: err,
	}
}

func (repo *stubTokenRepository) Create(_ context.Context, _ domain.Token) error {
	return repo.error
}

func (repo *stubTokenRepository) Get(_ context.Context, _ string) (*domain.Token, error) {
	return repo.token, repo.error
}
