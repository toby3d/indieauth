package token

import (
	"context"
	"net/url"

	"source.toby3d.me/toby3d/auth/internal/domain"
)

type (
	ExchangeOptions struct {
		ClientID     domain.ClientID
		RedirectURI  *url.URL
		Code         string
		CodeVerifier string
	}

	UseCase interface {
		Exchange(ctx context.Context, opts ExchangeOptions) (*domain.Token, *domain.Profile, error)

		// Verify checks the AccessToken and returns the associated information.
		Verify(ctx context.Context, accessToken string) (*domain.Token, *domain.Profile, error)

		// Revoke revokes the AccessToken and blocks its further use.
		Revoke(ctx context.Context, accessToken string) error
	}

	dummyTokenUseCase struct{}

	stubTokenUseCase struct {
		token   *domain.Token
		profile *domain.Profile
		error   error
	}
)

var (
	ErrRevoke error = domain.NewError(
		domain.ErrorCodeAccessDenied,
		"this token has been revoked",
		"",
	)
	ErrMismatchClientID error = domain.NewError(
		domain.ErrorCodeInvalidRequest,
		"client's URL MUST match the client_id used in the authentication request",
		"https://indieauth.net/source/#request",
	)
	ErrMismatchRedirectURI error = domain.NewError(
		domain.ErrorCodeInvalidRequest,
		"client's redirect URL MUST match the initial authentication request",
		"https://indieauth.net/source/#request",
	)
	ErrEmptyScope error = domain.NewError(
		domain.ErrorCodeInvalidScope,
		"empty scopes are invalid",
		"",
	)
	ErrMismatchPKCE error = domain.NewError(
		domain.ErrorCodeInvalidRequest,
		"code_verifier is not hashes to the same value as given in the code_challenge in the original "+
			"authorization request",
		"https://indieauth.net/source/#request",
	)
)

func NewDummyTokenUseCase() UseCase {
	return dummyTokenUseCase{}
}

func (dummyTokenUseCase) Exchange(_ context.Context, _ ExchangeOptions) (*domain.Token, *domain.Profile, error) {
	return nil, nil, nil
}

func (dummyTokenUseCase) Revoke(_ context.Context, _ string) error {
	return nil
}

func (dummyTokenUseCase) Verify(_ context.Context, _ string) (*domain.Token, *domain.Profile, error) {
	return nil, nil, nil
}

func NewStubTokenUseCase(token *domain.Token, profile *domain.Profile, err error) UseCase {
	return &stubTokenUseCase{
		token:   token,
		profile: profile,
		error:   err,
	}
}

func (ucase *stubTokenUseCase) Exchange(_ context.Context, _ ExchangeOptions) (*domain.Token, *domain.Profile, error) {
	return ucase.token, ucase.profile, ucase.error
}

func (ucase *stubTokenUseCase) Revoke(_ context.Context, _ string) error {
	return ucase.error
}

func (ucase *stubTokenUseCase) Verify(_ context.Context, _ string) (*domain.Token, *domain.Profile, error) {
	return ucase.token, ucase.profile, ucase.error
}
