package client

import (
	"context"

	"source.toby3d.me/toby3d/auth/internal/domain"
)

type (
	UseCase interface {
		// Discovery returns client public information bu ClientID URL.
		Discovery(ctx context.Context, id domain.ClientID) (*domain.Client, error)
	}

	dummyClientUseCase struct{}

	stubClientUseCase struct {
		client *domain.Client
		error  error
	}
)

var ErrInvalidMe error = domain.NewError(
	domain.ErrorCodeInvalidRequest,
	"cannot fetch client endpoints on provided me",
	"",
)

// NewDummyClientUseCase returns dummy [UseCase] what do nothing.
func NewDummyClientUseCase() UseCase {
	return dummyClientUseCase{}
}

func (dummyClientUseCase) Discovery(_ context.Context, _ domain.ClientID) (*domain.Client, error) {
	return nil, nil
}

// NewDummyClientUseCase returns stub [UseCase] what always returns data
// provided here.
func NewStubClientUseCase(client *domain.Client, err error) UseCase {
	return &stubClientUseCase{
		client: client,
		error:  err,
	}
}

func (ucase *stubClientUseCase) Discovery(_ context.Context, _ domain.ClientID) (*domain.Client, error) {
	return ucase.client, ucase.error
}
