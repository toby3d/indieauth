package client

import (
	"context"

	"source.toby3d.me/toby3d/auth/internal/domain"
)

type (
	Repository interface {
		Create(ctx context.Context, client domain.Client) error
		Get(ctx context.Context, cid domain.ClientID) (*domain.Client, error)
	}

	dummyClientRepository struct{}

	stubClientRepository struct {
		client *domain.Client
		error  error
	}
)

var ErrNotExist error = domain.NewError(
	domain.ErrorCodeInvalidClient,
	"client with the specified ID does not exist",
	"",
)

// NewDummyClientRepository creates a new dummy [Repository] implementation what
// do nothing.
func NewDummyClientRepository() Repository {
	return dummyClientRepository{}
}

func (dummyClientRepository) Create(_ context.Context, _ domain.Client) error {
	return nil
}

func (dummyClientRepository) Get(_ context.Context, _ domain.ClientID) (*domain.Client, error) {
	return nil, nil
}

// NewStubClientRepository creates a new stub [Repository] implementation what
// always return data provided here.
func NewStubClientRepository(client *domain.Client, err error) Repository {
	return &stubClientRepository{
		client: client,
		error:  err,
	}
}

func (repo *stubClientRepository) Create(_ context.Context, _ domain.Client) error {
	return repo.error
}

func (repo *stubClientRepository) Get(_ context.Context, _ domain.ClientID) (*domain.Client, error) {
	return repo.client, repo.error
}
