package http_test

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"golang.org/x/text/language"
	"golang.org/x/text/message"

	delivery "source.toby3d.me/toby3d/auth/internal/client/delivery/http"
	"source.toby3d.me/toby3d/auth/internal/domain"
	"source.toby3d.me/toby3d/auth/internal/random"
	"source.toby3d.me/toby3d/auth/internal/token"
	testutil "source.toby3d.me/toby3d/auth/internal/util/testing"
)

type Dependencies struct {
	token        *domain.Token
	client       *domain.Client
	config       *domain.Config
	matcher      language.Matcher
	tokenService token.UseCase
}

func TestHandler_ServeHTTP(t *testing.T) {
	t.Parallel()

	code, err := random.String(64)
	if err != nil {
		t.Fatal(err)
	}

	state, err := random.String(24)
	if err != nil {
		t.Fatal(err)
	}

	deps := NewDependencies(t)
	q := make(url.Values)

	for k, v := range map[string]string{
		"iss":   deps.client.ID.String(),
		"code":  code,
		"state": state,
	} {
		q.Add(k, v)
	}

	for name, target := range map[string]string{
		"home":     deps.client.ID.String(),
		"callback": deps.client.ID.URL().JoinPath("callback").String() + "?" + q.Encode(),
	} {
		name, target := name, target

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			req := httptest.NewRequest(http.MethodGet, target, nil)
			w := httptest.NewRecorder()

			delivery.NewHandler(delivery.NewHandlerOptions{
				Client:  *deps.client,
				Config:  *deps.config,
				Matcher: deps.matcher,
				Tokens:  deps.tokenService,
			}).ServeHTTP(w, req)

			resp := w.Result()

			if resp.StatusCode != http.StatusOK {
				t.Errorf("%s %s = %d, want %d", req.Method, req.RequestURI, resp.StatusCode,
					http.StatusOK)
			}

			testutil.GoldenEqual(t, w.Result().Body)
		})
	}
}

func NewDependencies(tb testing.TB) Dependencies {
	tb.Helper()

	tkn := domain.TestToken(tb)
	// WARN(toby3d): clients MUST NOT expect what any IndieAuth tokens is
	// JWT or something hackable. It can be anything in string
	// representation. Used static generated random string for safe golden
	// output comparing.
	tkn.AccessToken = "dqHhu6z0NcTy2trL7DuwXd0fMhLQ61ak"

	client := domain.TestClient(tb)
	config := domain.TestConfig(tb)
	matcher := language.NewMatcher(message.DefaultCatalog.Languages())
	tokenService := token.NewStubTokenUseCase(tkn, nil, nil)

	return Dependencies{
		token:        tkn,
		client:       client,
		config:       config,
		matcher:      matcher,
		tokenService: tokenService,
	}
}
