package http_test

import (
	"net/url"
	"testing"

	"github.com/google/go-cmp/cmp"

	httputil "source.toby3d.me/toby3d/auth/internal/util/http"
)

func TestParseLink(t *testing.T) {
	t.Parallel()

	for name, tc := range map[string]struct {
		input  string
		expect []httputil.Link
	}{
		"param": {
			input: `<https://example.com>; rel="preconnect"`,
			expect: []httputil.Link{{
				URL:    &url.URL{Scheme: "https", Host: "example.com"},
				Params: url.Values{"rel": []string{"preconnect"}},
			}},
		},
		"params": {
			input: `<https://example.com/%E8%8B%97%E6%9D%A1>; rel="preconnect"; priority=high`,
			expect: []httputil.Link{{
				URL:    &url.URL{Scheme: "https", Host: "example.com", Path: "/苗条"},
				Params: url.Values{"rel": []string{"preconnect"}, "priority": []string{"high"}},
			}},
		},
	} {
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			actual, err := httputil.ParseLink(tc.input)
			if err != nil {
				t.Fatal(err)
			}

			if diff := cmp.Diff(actual, tc.expect, cmp.AllowUnexported(url.URL{})); diff != "" {
				t.Error(diff)
			}
		})
	}
}
