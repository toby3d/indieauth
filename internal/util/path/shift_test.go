package path_test

import (
	"testing"

	pathutil "source.toby3d.me/toby3d/auth/internal/util/path"
)

func TestShift(t *testing.T) {
	t.Parallel()

	for in, out := range map[string][2]string{
		"/":         {"", "/"},
		"/foo":      {"foo", "/"},
		"/foo/":     {"foo", "/"},
		"/foo/bar":  {"foo", "/bar"},
		"/foo/bar/": {"foo", "/bar"},
	} {
		in, out := in, out

		t.Run(in, func(t *testing.T) {
			t.Parallel()

			head, path := pathutil.Shift(in)
			if out[0] != head || out[1] != path {
				t.Errorf("ShiftPath(%s) = '%s', '%s', want '%s', '%s'", in, head, path, out[0], out[1])
			}
		})
	}
}
