// Package common contains common constant strings constantly used in other
// packages.
//
// DO NOT import or provide anythyng except constants here.
package common

const charsetUTF8 string = "charset=UTF-8"

// Content types of the HTTP request and response body.
const (
	MIMEApplicationForm            string = "application/x-www-form-urlencoded"
	MIMEApplicationJSON            string = "application/json"
	MIMEApplicationJSONCharsetUTF8 string = MIMEApplicationJSON + "; " + charsetUTF8
	MIMETextHTML                   string = "text/html"
	MIMETextHTMLCharsetUTF8        string = MIMETextHTML + "; " + charsetUTF8
	MIMETextPlain                  string = "text/plain"
	MIMETextPlainCharsetUTF8       string = MIMETextPlain + "; " + charsetUTF8
)

// HTTP header names.
const (
	HeaderAccept                   string = "Accept"
	HeaderAcceptLanguage           string = "Accept-Language"
	HeaderAccessControlAllowOrigin string = "Access-Control-Allow-Origin"
	HeaderAuthorization            string = "Authorization"
	HeaderContentType              string = "Content-Type"
	HeaderCookie                   string = "Cookie"
	HeaderHost                     string = "Host"
	HeaderLink                     string = "Link"
	HeaderLocation                 string = "Location"
	HeaderVary                     string = "Vary"
	HeaderWWWAuthenticate          string = "WWW-Authenticate"
	HeaderXCSRFToken               string = "X-CSRF-Token"
)

// Searching class names.
const (
	HApp  string = "h-app"
	HCard string = "h-card"
	HXApp string = "h-x-app"
)

// Searching property names.
const (
	PropertyEmail string = "email"
	PropertyLogo  string = "logo"
	PropertyName  string = "name"
	PropertyPhoto string = "photo"
	PropertyURL   string = "url"
)

// Expected 'rel' attribute values.
const (
	RelAuthn                 string = "authn"
	RelAuthorizationEndpoint string = "authorization_endpoint"
	RelIndieAuthMetadata     string = "indieauth-metadata"
	RelMicropub              string = "micropub"
	RelMicrosub              string = "microsub"
	RelRedirectURI           string = "redirect_uri"
	RelTicketEndpoint        string = "ticket_endpoint"
	RelTokenEndpoint         string = "token_endpoint"
)

// Und used as String method output of custom types.
const Und string = "und"
