package http

import (
	"encoding/json"
	"net/http"

	"source.toby3d.me/toby3d/auth/internal/common"
	"source.toby3d.me/toby3d/auth/internal/domain"
)

type Handler struct {
	metadata *domain.Metadata
}

func NewHandler(metadata *domain.Metadata) *Handler {
	return &Handler{
		metadata: metadata,
	}
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != "" && r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)

		return
	}

	w.Header().Set(common.HeaderContentType, common.MIMEApplicationJSONCharsetUTF8)

	scopes, responseTypes, grantTypes, codeChallengeMethods := make([]string, 0), make([]string, 0),
		make([]string, 0), make([]string, 0)

	for i := range h.metadata.ScopesSupported {
		scopes = append(scopes, h.metadata.ScopesSupported[i].String())
	}

	for i := range h.metadata.ResponseTypesSupported {
		responseTypes = append(responseTypes, h.metadata.ResponseTypesSupported[i].String())
	}

	for i := range h.metadata.GrantTypesSupported {
		grantTypes = append(grantTypes, h.metadata.GrantTypesSupported[i].String())
	}

	for i := range h.metadata.CodeChallengeMethodsSupported {
		codeChallengeMethods = append(codeChallengeMethods,
			h.metadata.CodeChallengeMethodsSupported[i].String())
	}

	_ = json.NewEncoder(w).Encode(&MetadataResponse{
		Issuer:                h.metadata.Issuer.String(),
		AuthorizationEndpoint: h.metadata.AuthorizationEndpoint.String(),
		TokenEndpoint:         h.metadata.TokenEndpoint.String(),
		IntrospectionEndpoint: h.metadata.IntrospectionEndpoint.String(),
		IntrospectionEndpointAuthMethodsSupported: h.metadata.IntrospectionEndpointAuthMethodsSupported,
		RevocationEndpoint:                        h.metadata.RevocationEndpoint.String(),
		// NOTE(toby3d): If a revocation endpoint is provided, this
		// property should also be provided with the value ["none"],
		// since the omission of this value defaults to
		// client_secret_basic according to RFC8414.
		RevocationEndpointAuthMethodsSupported:     h.metadata.RevocationEndpointAuthMethodsSupported,
		ScopesSupported:                            scopes,
		ResponseTypesSupported:                     responseTypes,
		GrantTypesSupported:                        grantTypes,
		ServiceDocumentation:                       h.metadata.ServiceDocumentation.String(),
		CodeChallengeMethodsSupported:              codeChallengeMethods,
		AuthorizationResponseIssParameterSupported: h.metadata.AuthorizationResponseIssParameterSupported,
		UserinfoEndpoint:                           h.metadata.UserinfoEndpoint.String(),
	})

	w.WriteHeader(http.StatusOK)
}
