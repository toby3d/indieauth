{% package template %}

{% import (
  "source.toby3d.me/toby3d/auth/internal/domain"
  "source.toby3d.me/toby3d/auth/internal/domain/response"
  "source.toby3d.me/toby3d/auth/internal/domain/scope"
  "source.toby3d.me/toby3d/auth/web/template/layout"
) %}

{% code type Home struct {
  layout.BaseOf
  Client *domain.Client
  State  string
} %}

{% stripspace %}
{% func (p *Home) Head() %}
  {%= p.BaseOf.Head() %}
  {% for i := range p.Client.RedirectURI %}
    <link rel="redirect_uri" href="{%s p.Client.RedirectURI[i].String() %}">
  {% endfor %}
{% endfunc %}

{% func (p *Home) Body() %}
<header class="h-app h-x-app">
  {% if p.Client.Logo != nil %}
    <img class="u-logo"{% space %}
         src="{%s p.Client.Logo.String() %}"{% space %}
         alt="{%s p.Client.Name %}"{% space %}
         crossorigin="anonymous"{% space %}
         decoding="async"{% space %}
         height="140"{% space %}
         importance="high"{% space %}
         referrerpolicy="no-referrer-when-downgrade"{% space %}
         width="140">
  {% endif %}

  <h1><a class="p-name u-url" href="{%s p.Client.URL.String() %}">{%s p.Client.Name %}</a></h1>
</header>

<main>
  <form class=""{% space %}
        method="get"{% space %}
        action="/authorize"{% space %}
        enctype="application/x-www-form-urlencoded"{% space %}
        accept-charset="utf-8"{% space %}
        target="_self">

    {% for _, field := range [][2]string{
      {"client_id", p.Client.ID.String()},
      {"redirect_uri", p.Client.RedirectURI[0].String()},
      {"response_type", response.Code.String()},
      {"scope", domain.Scopes{scope.Email, scope.Profile}.String()},
      {"state", p.State},
    } %}
      <input type="hidden"{% space %}
             name="{%s field[0] %}"{% space %}
             value="{%s field[1] %}">
    {% endfor %}

    <input type="url"{% space %}
           name="me"{% space %}
           placeholder="https://example.com/"{% space %}
           inputmode="url"{% space %}
           autocomplete="url"{% space %}
           required>

    <button type="submit">{%= p.T("Sign In") %}</button>
  </form>
</main>
{% endfunc %}
{% endstripspace %}
