module source.toby3d.me/toby3d/auth

go 1.21

require (
	github.com/DATA-DOG/go-sqlmock v1.5.2
	github.com/brianvoe/gofakeit/v6 v6.28.0
	github.com/google/go-cmp v0.6.0
	github.com/jmoiron/sqlx v1.4.0
	github.com/lestrrat-go/jwx/v2 v2.0.21
	github.com/tomnomnom/linkheader v0.0.0-20180905144013-02ca5825eb80
	github.com/valyala/fasttemplate v1.2.2
	github.com/valyala/quicktemplate v1.7.0
	go.etcd.io/bbolt v1.3.10
	golang.org/x/exp v0.0.0-20240416160154-fe59bbe5cc7f
	golang.org/x/text v0.15.0
	golang.org/x/xerrors v0.0.0-20231012003039-104605ab7028
	inet.af/netaddr v0.0.0-20230525184311-b8eac61e914a
	modernc.org/sqlite v1.29.9
	source.toby3d.me/toby3d/form v0.4.0
	willnorris.com/go/microformats v1.2.0
)

require (
	github.com/goccy/go-json v0.10.2 // indirect
	github.com/hashicorp/golang-lru/v2 v2.0.7 // indirect
	github.com/ncruces/go-strftime v0.1.9 // indirect
	modernc.org/gc/v3 v3.0.0-20240304020402-f0dba7c97c2b // indirect
)

require (
	github.com/caarlos0/env/v10 v10.0.0
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.3.0 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/lestrrat-go/blackmagic v1.0.2 // indirect
	github.com/lestrrat-go/httpcc v1.0.1 // indirect
	github.com/lestrrat-go/httprc v1.0.5 // indirect
	github.com/lestrrat-go/iter v1.0.2 // indirect
	github.com/lestrrat-go/option v1.0.1 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	github.com/segmentio/asm v1.2.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	go4.org/intern v0.0.0-20230525184215-6c62f75575cb // indirect
	go4.org/unsafe/assume-no-moving-gc v0.0.0-20231121144256-b99613f794b6 // indirect
	golang.org/x/crypto v0.22.0 // indirect
	golang.org/x/net v0.24.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	modernc.org/libc v1.50.5 // indirect
	modernc.org/mathutil v1.6.0 // indirect
	modernc.org/memory v1.8.0 // indirect
	modernc.org/strutil v1.2.0 // indirect
	modernc.org/token v1.1.0 // indirect
)
